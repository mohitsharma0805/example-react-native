import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import config from '../config';

const HomeCell = ({ item }) => {
  return (
    <View style={styles.contentView}>
      <Text style={styles.text}>{item}</Text>
      <View style={styles.divider} />
    </View>
  );
};

export default HomeCell;

const styles = StyleSheet.create({
  contentView: {
    width: '100%',
    height: 44,
    justifyContent: 'center',
  },
  text: {
    marginTop: 5,
    fontSize: 17,
    marginLeft: 20,
    height: 30,
  },
  divider: {
    marginTop: 1,
    marginLeft: 12,
    height: 1,
    backgroundColor: config.colors.cellDivider,
  },
});
