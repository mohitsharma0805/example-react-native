import React, { useState } from 'react';

import { FlatList, StyleSheet, SafeAreaView, Keyboard } from 'react-native';
import { ExButton, ExTextInput } from '../components';
import HomeCell from './HomeCell';

const Home = () => {
  const [name, setName] = useState('');
  const [arrValue, setArrValue] = useState([]);

  const textChanged = (text) => {
    setName(text);
  };

  const buttonClicked = () => {
    Keyboard.dismiss();
    setArrValue(name.split(''));
  };

  const keyExtractor = (item, index) => `${item}${index}`;
  const renderItem = ({ item }) => {
    return <HomeCell item={item} />;
  };
  return (
    <SafeAreaView style={styles.contentView}>
      <ExTextInput
        viewStyle={styles.textInput}
        placeholder="Enter name"
        onChangeText={textChanged}
        autoCapitalize="words"
        autoCorrect={false}
      />
      <ExButton buttonStyle={styles.button} text="Submit" onPress={buttonClicked} />
      <FlatList
        style={styles.flatList}
        keyboardShouldPersistTaps="handled"
        data={arrValue}
        keyExtractor={keyExtractor}
        renderItem={renderItem}
      />
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  contentView: { flex: 1 },
  textInput: { marginTop: 20 },
  button: { marginTop: 20 },
  flatList: { flex: 1, marginTop: 20 },
});
