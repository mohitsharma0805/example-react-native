import React from 'react';
import { shallow } from 'enzyme';
import { View, Text } from 'react-native';
import HomeCell from '../HomeCell';

describe('HomeCell', () => {
  describe('Rendering', () => {
    const wrapper = shallow(<HomeCell />);
    it('renders the container view', () => {
      expect(wrapper).toMatchSnapshot();
      expect(wrapper.find(Text).length).toEqual(1);
      expect(wrapper.find(View).length).toEqual(2);
    });
  });
});
