import React from 'react';
import { shallow } from 'enzyme';
import { SafeAreaView, FlatList } from 'react-native';
import Home from '../Home';
import { ExTextInput, ExButton } from '../../components';

describe('Home', () => {
  describe('UI test cases', () => {
    const wrapper = shallow(<Home />);
    it('renders the main view', () => {
      expect(wrapper).toMatchSnapshot();
      expect(wrapper.find(SafeAreaView).length).toEqual(1);
      expect(wrapper.find(FlatList).length).toEqual(1);
      expect(wrapper.find(ExTextInput).length).toEqual(1);
      expect(wrapper.find(ExButton).length).toEqual(1);
    });

    it('Simulate TextInput and Button', () => {
      const textInput = wrapper.find(ExTextInput).first();
      textInput.simulate('changeText', 'Mohit');

      const button = wrapper.find(ExButton);
      button.simulate('press');
      expect(button).toBeTruthy();
    });
  });
});
