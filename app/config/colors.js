export default {
  appTheme: '#10A5DA',
  white: '#FFFFFF',
  black: '#000000',
  gray: '#999999',
  cellDivider: '#E8E8E8',
};
