import colors from './colors';
import local from './local';

const config = {
  colors,
  ...local,
};

export default config;
