import React from 'react';
import config from './app/config';
import StorybookUIRoot from './storybook';
import { Home } from './app/screens';

const App = () => {
  if (config.storybookEnabled) {
    return <StorybookUIRoot />;
  }
  return <Home />;
};

export default App;
